(Re)issuing a certificate with Let's Encrypt
=

Install the requirements from `Pipfile`

If you haven't acquired a key from Let's Encrypt before, then run:
```bash 
manuale register yourname@vacansoleil.com
```
---

After that, you can authorize 1 or more domains (you might be asked to add TXT records for these domains:

```bash
manuale authorize domain1.com [domain2.com ...]
```
---
Issue the certificate in a `certs/` subfolder:

```bash
manuale issue --output certs/ domain1.com [domain2.com ...]
```
